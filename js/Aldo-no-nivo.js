﻿/// <reference path="../fr/artists/abbagliati.html" />
/// <reference path="jquery-1.9.1.intellisense.js" />


//When Ready
$(function () {
    //Clear modal
    $('body').on('hidden.bs.modal', '.modal', function () {
        $(this).removeData('bs.modal');
    });


    //Related articles
    //Make active
    $('.panel>.list-group .list-group-item').click(function (e) {
        $('.panel>.list-group .list-group-item').removeClass('active')
        $(this).addClass('active')
    })



    //Get ajax on click
    //nl
    $(".panel>.list-group .list-group-item#nl-revue-blanche").click(function () {
        //Get Data
        $.ajax("nl/artists/revue-blanche.html#artists")
        .done(function (data) {
            //show data
            $('#getAjax').html(data);
            //$('#getAjax').append("Ajax Request done ! <br /><br />");

        })
        .error(function (request) {
            $('#getAjax').append("Oeps ! Een Foutje ! <br /><br />");
            $('#getAjax').append(request.responseText);
        });


    });
    $(".panel>.list-group .list-group-item#nl-abbagliati").click(function () {
        //Get Data
        $.ajax("nl/artists/abbagliati.html")
        .done(function (data) {
            //show data
            $('#getAjax').html(data);
            //$('#getAjax').append("Ajax Request done ! <br /><br />");

        })
        .error(function (request) {
            //$('#getAjax').append("Oeps ! Een Foutje ! <br /><br />");
            //$('#getAjax').append(request.responseText);
        });


    });
    $(".panel>.list-group .list-group-item#nl-triokhaldei").click(function () {
        //Get Data
        $.ajax("nl/artists/trio-khaldei.html")
        .done(function (data) {
            //show data
            $('#getAjax').html(data);
            //$('#getAjax').append("Ajax Request done ! <br /><br />");

        })
        .error(function (request) {
            //$('#getAjax').append("Oeps ! Een Foutje ! <br /><br />");
            //$('#getAjax').append(request.responseText);
        });


    });
    $(".panel>.list-group .list-group-item#nl-kinderconcerten").click(function () {
        //Get Data
        $.ajax("nl/artists/kinderconcerten.html#artists")
        .done(function (data) {
            //show data
            $('#getAjax').html(data);
            //$('#getAjax').append("Ajax Request done ! <br /><br />");

        })
        .error(function (request) {
            //$('#getAjax').append("Oeps ! Een Foutje ! <br /><br />");
            //$('#getAjax').append(request.responseText);
        });


    });

    
    //Engels en frans  
    $(".panel>.list-group .list-group-item#revue-blanche").click(function () {
        //Get Data
        $.ajax("artists/revue-blanche.html")
        .done(function (data) {
            //show data
            $('#getAjax').html(data);
            //$('#getAjax').append("Ajax Request done ! <br /><br />");

        })
        .error(function (request) {
            $('#getAjax').append("Oeps ! Een Foutje ! <br /><br />");
            $('#getAjax').append(request.responseText);
        });
    });
    $(".panel>.list-group .list-group-item#abbagliati").click(function () {
        //Get Data
        $.ajax("artists/abbagliati.html")
        .done(function (data) {
            //show data
            $('#getAjax').html(data);
            //$('#getAjax').append("Ajax Request done ! <br /><br />");

        })
        .error(function (request) {
            $('#getAjax').append("Oeps ! Een Foutje ! <br /><br />");
            $('#getAjax').append(request.responseText);
        });
    });
    $(".panel>.list-group .list-group-item#triokhaldei").click(function () {
        //Get Data
        $.ajax("artists/trio-khaldei.html")
        .done(function (data) {
            //show data
            $('#getAjax').html(data);
            //$('#getAjax').append("Ajax Request done ! <br /><br />");

        })
        .error(function (request) {
            $('#getAjax').append("Oeps ! Een Foutje ! <br /><br />");
            $('#getAjax').append(request.responseText);
        });
    });
    $(".panel>.list-group .list-group-item#kinderconcerten").click(function () {
        //Get Data
        $.ajax("artists/kinderconcerten.html")
        .done(function (data) {
            //show data
            $('#getAjax').html(data);
            //$('#getAjax').append("Ajax Request done ! <br /><br />");

        })
        .error(function (request) {
            $('#getAjax').append("Oeps ! Een Foutje ! <br /><br />");
            $('#getAjax').append(request.responseText);
        });
    });

    //opacity img
    $('img').animate({ "opacity": .5 });
    $('img-ico').animate({ "opacity": 1 })
    $('img').hover(
    //Over
        function () {
            $(this).stop().animate({
                "opacity": 1,
                'boxShadowX': '10px',
                'boxShadowY': '10px',
                'boxShadowBlur': '20px'
            });
        }
    //Out
    , function () {
        $(this).stop().animate({ "opacity": .5 });
    }
     );


});

